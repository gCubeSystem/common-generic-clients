package org.gcube.common.clients;

import org.gcube.common.clients.cache.DefaultEndpointCache;
import org.gcube.common.clients.cache.EndpointCache;

import jakarta.xml.ws.EndpointReference;

/**
 * Library-wide utilities
 * 
 * @author Fabio Simeoni
 *
 */
public class Utils {

	/**
	 * Shared endpoint cache.
	 */
	public static EndpointCache<EndpointReference> globalCache = new DefaultEndpointCache<EndpointReference>();
}
